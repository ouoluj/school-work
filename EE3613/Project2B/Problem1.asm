.data
strA:     .asciiz "Original Array:\n "
strB:     .asciiz "Second Array:\n "
newline:  .asciiz "\n"
space:    .asciiz " "

# This is the start of the original array.
Original: .word 200, 270, 250, 100
          .word 205, 230, 105, 235
          .word 190, 95, 90, 205
          .word 80, 205, 110, 215
# The next statement allocates room for the other array.
# The array takes up 4*16=64 bytes.
#
Second: .space 64

.align 2
.globl main
.text
main:   la $s0, Original # Initialize source address register
        la $s1, Second   # Initialize destination address register

        li $t0, 0 # Initialize register for start of loop
        li $t1, 4 # Initialize register for end of loop

        # BEGINNING OF INVERSION
Loop:   addi $t0, $t0, 1 # Increment counter
        lw $s3, 0($s0) # Load word from original array
        sw $s3, 0($s1) # Store word in new array
        addi $s0, $s0, 16 # Increment to next index in Original column
        addi $s1, $s1, 4  # Increment to next index in Second row

        lw $s3, 0($s0) # Load word from original array
        sw $s3, 0($s1) # Store word in new array
        addi $s0, $s0, 16 # Increment to next index in Original column
        addi $s1, $s1, 4  # Increment to next index in Second row

        lw $s3, 0($s0) # Load word from original array
        sw $s3, 0($s1) # Store word in new array
        addi $s0, $s0, 16 # Increment to next index in Original column
        addi $s1, $s1, 4  # Increment to next index in Second row

        lw $s3, 0($s0) # Load word from original array
        sw $s3, 0($s1) # Store word in new array

        mul $t2, $t0, $t1 # Multiply iteration by four to get start of Original's next column
        la $s0, Original # Reinitialize cursor in Original
        add $s0, $s0, $t2 # Start cursor at next column of Original
        addi $s1, $s1, 4  # Increment to next index in Second row
        bne $t0, $t1, Loop


        # BEGINNING OF PRINT
        # Print label for original array
        li $v0, 4 # Load Print_string
        #
        la $a0, space # Pass address for space to maintain consistency
        syscall
        #
        la $a0, strA  # Pass address for array label (.strA)
        syscall

        li $t2, 0 # Initialize counter for columns
        li $t3, 0 # Initialize counter for rows
        la $s0, Original # Initialize cursor for array, starting with Original

Print:
        lw $s1, 0($s0) # Load next word to output register

        # Output $s0
        li $v0, 1 # Load print_int
        move $a0, $s1 # Load number at current output cursor position
        syscall
        # Output space to separate numbers
        li $v0, 4 # Load Print_string
        la $a0, space # Pass address for space
        syscall
        #
        addi $t2, 1 # Increment column counter after each is printed
        addi $s0, 4 # Increment cursor to next word

        bne $t2, $t1, Print # Print numbers until fourth column is printed, then continue to begin next row
        # Output newline between rows
        li $v0, 4 # Load Print_string
        la $a0, newline  # Pass address for newline character
        syscall
        #
        la $a0, space # Pass address for space to maintain consistency
        syscall
        #
        addi $t3, 1 # Increment row counter after each is finished
        li $t2, 0   # Reinitialize column counter after starting new row
        bne $t3, $t1, Print # Print rows until fourth is finished, then continue on

        beqz $t0, End # For Original $t0 == 4. Jump to end if equal zero, continue to Second array if not
        li $t0, 0 # $t0 = 0 for checking at the end of Second

        # Print label for second array
        li $v0, 4 # Load Print_string
        la $a0, strB # Pass address for second array label (.strB)
        syscall
        #

        la $s0, Second # Initialize cursor for second array
        li $t2, 0 # Reinitialize colomn counter
        li $t3, 0 # Reinitialize row counter
        j Print


End:    li $v0, 10 # Load exit
        syscall
