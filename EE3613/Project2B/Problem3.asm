.data
prompt: .asciiz "Please enter the nth Fibonacci number to be displayed: " # Load prompt string
comma:  .asciiz ", " # Load string ", " for output

.globl main
.text
main: # Print_string "Please enter the nth Fibonacci number to be displayed:" (sys call 4) $a0
      li $v0, 4 # Load Print_string
      la $a0, prompt  # Pass address for prompt
      syscall
      # Read_int (sys call 5) $v0
      li $v0, 5 # Load read_int
      syscall
      move $t1, $v0 # Initialize register $t1 for number of iterations

      li $t0, 0 # Initialize $t0 as counter
      li $s0, 0 # Initialize register for n-2
      li $s1, 1 # Initialize register for n-1

      beq $t0, $t1, End # Branch if n == 0
      # Output $s0
      li $v0, 1 # Load print_int
      move $a0, $s0 # Load first number (0)
      syscall
      #

      addi $t0, $t0, 1 # Increment counter (n += 1)

      beq $t0, $t1, End # Branch if n == 1
      # Output string if continuing past previous number
      li $v0, 4 # Load Print_string
      la $a0, comma # Pass address for comma and space
      syscall
      # Output $s1
      li $v0, 1 # Load print_int
      move $a0, $s1 # Load second number (1)
      syscall
      #
      addi $t0, $t0, 1 # Increment counter (n += 1)

Loop: beq $t0, $t1, End # Branch when i == n
      # Output string only if loop is not finished
      li $v0, 4 # Load Print_string
      la $a0, comma # Pass address for comma and space
      syscall
      #
      add $s2, $s0, $s1
      # Output $s2
      li $v0, 1 # Load print_int
      move $a0, $s2 # Load current number ((n-1) + (n-2))
      syscall
      #
      move $s0, $s1 # Move $s1 to $s0
      move $s1, $s2 # Move $s1 to $s0
      addi $t0, $t0, 1 # Increment counter (n += 1)
      j Loop

End:  li $v0, 10 # Load exit
      syscall
