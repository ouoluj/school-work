.data
.strA:    .asciiz "Please enter your choice to skip numbers (1-4)\n"
Numbers:  .byte   100, -7, 11, 25, -66, 99, -1, 34, 12, 22, -2, -7, 100, 11, 4, 67, 2, -90, 22, 2, 56, 3, -89, 12, -10, 21, 10, -25, -6, 9, 111, 34, 12, 22, -2, -17, 100, 111, -4, 7, 14, -19, -2, 29, 36, 31, -79, 2

.globl main
.text
main:   # Print_string
        li $v0, 4 # Load Print_string
        la $a0, .strA  # Pass address for prompt (.strA)
        syscall
        # Read_int (sys call 5) $v0
        li $v0, 5 # Load read_int
        syscall
        move $t0, $v0 # Initialize register $t0 with skip number

        la $s0, Numbers # Initialize "cursor" for numbers array
        addi $s2, $s0, 48
        li $s1, 0 # Initialize sum

Loop:   lb $t1, 0($s0) # Move next number to working register
        add $s1, $s1, $t1 # Add next number to current sum (sum += Numbers[i])
        add $s0, $s0, $t0 # Move cursor by skip number

        slt $t2, $s0, $s2 # Set $t2 to 1 if cursor < end of array, 0 if cursor is at end of array
        bnez $t2, Loop

End:    li $v0, 1 # Load print_int
        move $a0, $s2 # Load current number ((n-1) + (n-2))
        syscall
        #
        li $v0, 10 # Load exit
        syscall
