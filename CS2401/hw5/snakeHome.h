//****************************************************************
//
//  Class:   snakeHome
//
//  Purpose: A child class of Home, suitable for home-ing a snake.
//
//****************************************************************
#ifndef SNAKE_H
#define SNAKE_H
#include "home.h"

class snakeHome:public Home {
public:
  snakeHome();
  void input(std::istream& ins);
  void output(std::ostream& outs);

private:
  bool venomous; // Is the snake venomous?
  int depth; // Depth of hole
  double temp; // Temperature of dirt
};
#endif
