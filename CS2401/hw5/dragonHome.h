//****************************************************************
//
//  Class:   dragonHome
//
//  Purpose: A child class of Home, suitable for home-ing a dragon.
//
//****************************************************************
#ifndef DRAGON_H
#define DRAGON_H
#include "home.h"

class dragonHome:public Home {
public:
  dragonHome();
  void input(std::istream& ins);
  void output(std::ostream& outs);

private:
  double diameter; // Diameter of main section of cave.
  int height; // Height of main section of cave.
  double value; // Value of the dragon's hoard.
};
#endif
