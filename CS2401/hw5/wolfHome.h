//****************************************************************
//
//  Class:   wolfHome
//
//  Purpose: A child class of Home, suitable for home-ing a wolf.
//
//****************************************************************
#ifndef WOLF_H
#define WOLF_H
#include "home.h"

class wolfHome:public Home {
public:
  wolfHome();
  void input(std::istream& ins);
  void output(std::ostream& outs);

private:
  int width; // One dimension of the territory.
  int length; // Other dimension of the territory.
  size_t occupancy; // Number of wolves in the territory
};
#endif
