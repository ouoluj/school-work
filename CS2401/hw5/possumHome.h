//****************************************************************
//
//  Class:   possumHome
//
//  Purpose: A child class of Home, suitable for home-ing a possum.
//
//****************************************************************
#ifndef POSSUM_H
#define POSSUM_H
#include "home.h"

class possumHome:public Home {
public:
  possumHome();
  void input(std::istream& ins);
  void output(std::ostream& outs);

private:
  std::string type; // Type of trash can
  double capacity; // Capacity of trash can
  double diameter; // Diameter of entrance
};
#endif
