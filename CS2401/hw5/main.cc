//****************************************************************
//
//  Program:        Main for project 5
//
//  Name:           Derek Baker
//  Email:          db820017@ohio.edu
//
//  Section:        100 (John Dolan)
//
//  Date:           4-14-19
//
//****************************************************************
#include <iostream>
#include <list>
#include <iterator>
#include <fstream>

#include "wolfHome.h"
#include "dragonHome.h"
#include "hawkHome.h"
#include "possumHome.h"
#include "snakeHome.h"

using namespace std;

void load(list<Home*>& homes);
void close(list<Home*>& homes);
int menu();
void show_list(ostream& outs, list<Home*>& homes);

int main() {
  list<Home*> homes;
  load(homes);
  int choice = 0;

  Home *tmp;
  choice = menu();
  while (choice != 9) {
    switch (choice) {
      case 1:
        tmp = new wolfHome;
        tmp->input(cin);
        // wolfHome *w;
        // w.input();
        homes.push_back(tmp);
        break;

      case 2:
        tmp = new dragonHome;
        tmp->input(cin);
        homes.push_back(tmp);
        break;

      case 3:
        tmp = new hawkHome;
        tmp->input(cin);
        homes.push_back(tmp);
        break;

      case 4:
        tmp = new possumHome;
        tmp->input(cin);
        homes.push_back(tmp);
        break;

      case 5:
        tmp = new snakeHome;
        tmp->input(cin);
        homes.push_back(tmp);
        break;

      case 6:
        show_list(cout, homes);
        break;

      case 9:
        break;

      default:
        cout << "Not a valid selection! Please try again." << endl;
        break;
    }

    choice = menu();
  }

  close(homes);

  return 0;
}


void load(list<Home*>& homes) {
  string type = "";
  ifstream ifs;
  ifs.open("database.dat");
  if (ifs.fail())
    {std::cout << "Error reading input file." << '\n'; return;}
  Home *load;

  getline(ifs, type);
  while (!ifs.eof()) {
    if (type == "wolf")
      {load = new wolfHome;}
    else if (type == "dragon")
      {load = new dragonHome;}
    else if (type == "hawk")
      {load = new hawkHome;}
    else if (type == "possum")
      {load = new possumHome;}
    else if (type == "snake")
      {load = new snakeHome;}
    else {
      std::cout << "Error" << '\n';
      getline(ifs, type);
      continue;
    }
    load->input(ifs);
    homes.push_back(load);

    getline(ifs, type);
  }
  ifs.close();
}

void close(list<Home*>& homes) {
  ofstream outs;
  outs.open("database.dat");
  if (outs.fail()) {
    cout << "Failed to save backup file." << endl;
    return;
  }
  show_list(outs,homes);
  outs.close();
}

int menu() {
  int tmp = 0;
  cout << "What kind of house would you like to build?" << endl;
  cout << " 1: Wolf" << endl;
  cout << " 2: Dragon" << endl;
  cout << " 3: Hawk" << endl;
  cout << " 4: Possum" << endl;
  cout << " 5: Snake" << endl;
  cout << "Enter 6 to see all the homes that have been ordered" << endl;
  cout << "Enter 9 to exit the program" << endl;
  cin >> tmp;
  return tmp;
}

void show_list(ostream& outs, list<Home*>& homes) {
  list<Home*>::iterator it;
  if (homes.empty()) {cout << "List is empty!" << endl; return;}
  for (it = homes.begin(); it != homes.end(); it++) {
    (*it)->output(outs);
  }
}
