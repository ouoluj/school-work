//****************************************************************
//
//  Class:   hawkHome
//
//  Purpose: A child class of Home, suitable for home-ing a hawk.
//
//****************************************************************
#ifndef HAWK_H
#define HAWK_H
#include "home.h"

class hawkHome:public Home {
public:
  hawkHome();
  void input(std::istream& ins);
  void output(std::ostream& outs);

private:
  std::string type; // Type of tree
  int height; // Height of tree
  double diameter; // Diameter of nest
};
#endif
