//****************************************************************
//
//  Class:   Carlist
//
//  Purpose: Implementation file for the carlist class.
//
//
//****************************************************************
#include "carlist.h"

Carlist::Carlist() {
  head = NULL;
}

Carlist::Carlist(const Carlist& other) {
  if(other.head == NULL) {head = NULL;}
  else {
    head = new node(other.head->data(), NULL);
    node *sptr = other.head;
    node *dptr = head;

    sptr = sptr->link();
    while (sptr != NULL) {
      dptr->set_link(new node(sptr->data(), NULL));
      dptr = dptr->link();
      sptr = sptr->link();
    }
  }
}

void Carlist::operator =(const Carlist& other) {
  if (this == &other) {return;}

  // Delete current Carlist
  node *del = head;
  node *current = head->link();
  while (current != NULL) {
    head = current;
    delete del;
    del = current;
    current = current->link();
  }

  // Create empty list if other list is empty
  if (other.head == NULL) {head = NULL; return;}

  // Copy other list to current list.
  head = new node(other.head->data(), NULL);
  node *sptr = other.head; // source pointer
  node *dptr = head; // destination pointer

  sptr = sptr->link();
  while (sptr != NULL) {
    dptr->set_link(new node(sptr->data(), NULL));
    dptr = dptr->link();
    sptr = sptr->link();
  }
}

Carlist::~Carlist() {
  node *del = head;
  node *current = head->link();
  while (current != NULL) {
    head = current;
    delete del;
    del = current;
    current = current->link();
  }
}

void Carlist::add(Car c) {
  if (head == NULL) {head = new node(c, NULL);}
  else {
    node *previous = head;
    node *current = head->link();
    if (c < head->data()) {
      node *tmp = new node(c, head);
      head = tmp;
    }
    else {
      while (current != NULL) {
        if (c < current->data()) {
          previous->set_link(new node(c, current));
          return;
        }
        previous = previous->link();
        current = current->link();
      }
      previous->set_link(new node(c, NULL));
    }
  }
}

void Carlist::showall(std::ostream& outs)const {
  node *cursor = head;
  while (cursor != NULL) {
    std::cout << cursor->data() << '\n';
    cursor = cursor->link();
  }
}

void Carlist::view_all_of_year(int target)const {
  node *cursor = head;
  while (cursor != NULL) {
    if (cursor->data().get_year() == target) {
      std::cout << cursor->data() << '\n';
    }
    cursor = cursor->link();
  }
}

void Carlist::view_all_of_make(std::string target)const {
  node *cursor = head;
  while (cursor != NULL) {
    if (cursor->data().get_make() == target) {
      std::cout << cursor->data() << '\n';
    }
    cursor = cursor->link();
  }
}

Car Carlist::cheapest()const {
  node *cursor = head;
  Car cheapest = head->data();

  while (cursor != NULL) {
    if (cursor->data().get_price() < cheapest.get_price()) {
      cheapest = cursor->data();
    }
    cursor = cursor->link();
  }
  return cheapest;
}

double Carlist::tax_value()const {
  double total = 0, num = 0, value = 0;
  node *cursor = head;

  while (cursor != NULL) {
    total += cursor->data().get_price();
    num ++;
    cursor = cursor->link();
  }
  value = total/num;
  return value;
}

double Carlist::miles_per_year()const {
  node *cursor = head;
  double total = 0, num = 0;
  while (cursor != NULL) {
    int tmpMiles = cursor->data().get_miles();
    int tmpYear = cursor->data().get_year();
    if ((2019 - tmpYear) == 0) {
      total += tmpMiles;
      num ++;
    }
    else {
      total += tmpMiles / (2019 - tmpYear);
      num ++;
    }
    cursor = cursor -> link();
  }
  return (total/num);
}

void Carlist::remove(Car c) {
  node *cursor = head;
  if (head->data() == c) {
    head = head->link();
    delete cursor;
  }

  else {
    node *previous = head;
    cursor = head->link();
    while (cursor != NULL) { // Cursor is already initialized to head's link, which, if null, will not enter loop
      if (cursor->data() == c) {
        previous->set_link(cursor->link());
        delete cursor;
      }
      previous = previous->link();
      cursor = cursor->link();
    }
  }
}

void Carlist::load(std::ifstream& ins) {
  Car tmp;
  ins >> tmp;
  while (!ins.eof()) {
    add(tmp);
    ins >> tmp;
  }
}

void Carlist::save(std::ofstream& outs) {
  node *cursor = head;
  Car temp;
  while (cursor != NULL) {
    temp = cursor -> data();
    outs << temp;
    cursor = cursor->link();
  }
}
