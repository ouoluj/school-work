//****************************************************************
//
//  Class:   Mancala
//
//  Purpose: Implementation for the mancala class, child of the game class.
//
//
//****************************************************************
#include "mancala.h"
#include "game.h"
#include <iostream>

using namespace std;
namespace main_savitch_14 {

Mancala::Mancala() {
  board[0][0] = board [1][0] = 0;
  for (size_t i = 1; i < 7; i++) {
    board[0][i] = board[1][i] = 4;
  }
}

game* Mancala::clone()const {return new Mancala(*this);}

void Mancala::compute_moves(queue<string>& moves)const {}

void Mancala::make_move(const std::string& move) {
  if (!is_legal(move)) {return;}
  int row = 1;
  int cup = (move[0] - '0');
  int gemNumber = board[row][cup];
  bool freeMove = false;

  board[row][cup] = 0;
  cup ++;
  for (int i = 0; i < gemNumber; i++) {
    if (cup == 7) {
      cup = 6;
      board[row][0]++;
      row = (row + 1) % 2;
    }

    else {
      if (row == 0) {
        board[row][cup]++;
        cup--;
      }

      if (row == 1) {
        board[row][cup]++;
        cup++;
      }
    }
  }

  if (freeMove == true)
    {cout << "Free move!" << endl;}
  else
    {game::make_move(move);}

  if (is_game_over()) {
    for (int i = 1; i < 7; i++) {
      board[0][0] += board [0][i];
      board[0][i] = 0;
      board[1][0] += board [1][i];
      board[1][i] = 0;
    }
  }
}

bool Mancala::is_game_over()const{
  bool p1 = true;
  bool p2 = true;
  for (size_t i = 0; i < 6; i++) {
    if (board[0][i] > 0) {p1 = false;}
    if (board[1][i] > 0) {p2 = false;}
  }
  return (p1 || p2);
}

// game::who Mancala::winning(game::who player)const {}

void Mancala::display_status()const {
  cout << endl;
  cout << GREEN << "   |";
  for (size_t i = 1; i < 7; i++) {
    cout << board[0][i] << "|";
  }
  cout /*<< "|   "*/ << endl;
  cout << "  " << RED << board[0][0] << "             " << BLUE << board[1][0] << endl;
  cout << GREEN << "   |";
  for (size_t i = 1; i < 7; i++) {
    cout << board[1][i] << "|";
  }
  cout << endl << endl;
}

int Mancala::evaluate()const {return 0;}

bool Mancala::is_legal(const string& move)const {
  int cup = (move[0] - '0');
  return cup <= 6 && cup >= 1; // && board[1][cup] != 0;
}

} // End of namespace main_savitch_14
