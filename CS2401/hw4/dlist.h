//****************************************************************
//  Name:    Derek Baker
//  Class:   dlist
//
//  Purpose: The header of the dlist class.
//
//****************************************************************

#ifndef DLIST_H
#define DLIST_H
#include <iostream>
#include "dnode.h"
#include "iterator.h"

template <class T>
class dlist {
public:

  dlist();
  dlist(dlist& other);
  ~dlist();
  dlist<T>& operator =(dlist& other);

  int size() {return nodecount;}

  void show();
  void reverse_show();

  void front_insert(T item);
  void rear_insert(T item);

  void front_remove();
  void rear_remove();


  typedef nodeiterator<T> iterator;
  iterator begin() {return iterator(head);}
  iterator r_begin() {return iterator(tail);}
  iterator end() {return iterator(NULL);}
  iterator r_end() {return iterator(NULL);}

  void remove(iterator& it);
  void insert_before(iterator& it, T item);
  void insert_after(iterator& it, T item);

private:
  dnode<T> *head;
  dnode<T> *tail;
  int nodecount;

};
#include "dlist.tmp"
#endif
