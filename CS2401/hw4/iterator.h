//****************************************************************
//  Name:    Derek Baker
//  Class:   iterator
//
//  Purpose: An external iterator for use with the dlist class.
//
//****************************************************************

#ifndef ITERATOR_H
#define ITERATOR_H

template <class T>
class dlist;

#include "dnode.h"

template <class T>
class nodeiterator {
public:
  friend class dlist<T>;

  nodeiterator(dnode<T> *init = NULL)
    {current = init;}

  bool operator !=(const nodeiterator &other)
    {return current != other.current;}

  bool operator ==(const nodeiterator &other)
    {return current == other.current;}

  T operator *()
    {return current -> data();}

  nodeiterator operator ++() {
    current = current -> next();
    return *this;
  }

  nodeiterator operator ++(int) {
    nodeiterator original(*this);
    current = current -> next();
    return original;
  }

  nodeiterator operator --() {
    current = current -> previous();
    return *this;
  }

  nodeiterator operator --(int) {
    nodeiterator original(*this);
    current = current -> previous();
    return original;
  }

private:
  dnode<T> *current;

};
#endif
