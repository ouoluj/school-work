#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int findMedian(vector<int>& list, int first, int middle, int last) {
  if ((list[middle] <= list[first] && list[first] <= list[last]) || (list[middle] >= list[first] && list[first] >= list[last])) {
    return first;
  }
  else if ((list[first] <= list[last] && list[last] <= list[middle]) || (list[first] >= list[last] && list[last] >= list[middle])) {
    return last;
  }
  else {
    return middle;
  }
}

int partition(vector<int>& list, int first, int last) {
  // The pivot should be the median of the
  // first, middle, and last elements.
  int median = findMedian(list, first, (first+last)/2, last);
  int pivot = list[median];
  swap(list[first], list[median]); // Move pivot to the front

  int smallIndex = first; // Set divider to the front

  for (int i = first + 1; i <= last; i++) {
    if (list[i] < pivot) { // Content check
      smallIndex++; // Move divider to the next spot
      swap(list[smallIndex], list[i]); // Swap current index into smaller sublist
    }
  }

  swap(list[first], list[smallIndex]); // Swap pivot back to between both sublists

  return smallIndex;
}

void quicksort(vector<int>& list, int first, int last) {
  if (first >= last) { return; } // Base case
  else {
    int middle = partition(list, first, last);
    quicksort(list, first, middle-1);
    quicksort(list, middle+1, last);
  }
}

void multiway_merge(vector<vector<int> >& input_lists, vector<int>& output_list) {
  vector<int>::iterator it; // Initialize erase iterator

  // Heapify outer vector using first element of each sublist
  make_heap(input_lists.begin(), input_lists.end(), greater<>{});

  for(size_t i = 0; i < output_list.size(); i++) {
    output_list[i] = (input_lists[0][0]); // Add smallest element to output_list

    pop_heap(input_lists.begin(), input_lists.end()); // Pop smallest element to back
    it = input_lists[input_lists.size()-1].begin(); // Set iterator to first element of last sublist
    input_lists[input_lists.size()-1].erase(it); // Erase used value
    
    if (input_lists[input_lists.size()-1].empty()) { // Check if sublist is empty after erase. If so, pop empty
      input_lists.pop_back();
    }

    make_heap(input_lists.begin(), input_lists.end(), greater<>{}); // Remake heap with new value from the sublist
  }
}

int main(int argc, char** argv) {
  int n, m;
  cin >> n >> m;

  vector<vector<int> > input_lists(n, vector<int>(m));

  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      cin >> input_lists[i][j];
    }
  }

  // Quicksort k sublists
  for (int i = 0; i < input_lists.size(); ++i)
    quicksort(input_lists[i], 0, m-1);

  // Merge n input sublists into one sorted list
  vector<int> output_list(n * m);
  multiway_merge(input_lists, output_list);

  for (int i = 0; i < output_list.size(); ++i)
    cout << output_list[i] << " ";
  cout << endl;
}
