//****************
// 
// A simple file created to test functions provided by <algorithm>, and to test vector functions important to project 4
// 
//****************

#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
using namespace std;

// Function to make printing the list easier
void print(vector< vector<int> > input_lists, int n, int m, string message);

int main(int argc, char** argv) {
  int n, m;
  // n is number of sublists; m is elements in each sublist
  cin >> n >> m;

  vector<vector<int> > input_lists(n, vector<int>(m));

  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      // Input and cout each sublist as it's built
      cin >> input_lists[i][j];
      // cout << input_lists[i][j] << ' ';
    }
    // Separate each sublist
    // cout << endl;
  }

  
  // Make the outside vector a heap
  make_heap(input_lists.begin(), input_lists.end(), greater<>{});
  print(input_lists, n, m, "Heapified");

  // Pop the smallest element, moving entire sublist to the back of the outside vector
  pop_heap(input_lists.begin(), input_lists.end());
  print(input_lists, n, m, "Popped");

  // Create iterator to delete the smallest element in heap. Sublist it came from shifts forward to fill
  vector<int>::iterator it = input_lists[n-1].begin();
  // input_lists[n-1].erase(it);
  // print(input_lists, n, m, "Erase");

  // push_heap(input_lists.begin(), input_lists.end(), greater<>{});
  // print(input_lists, n, m, "push_heap");

  make_heap(input_lists[0].begin(), input_lists[0].end(), greater<>{});
  print(input_lists, n, m, "Test loop");
  for(int i = 0; i < (n*m); i++) {
    pop_heap(input_lists.begin(), input_lists.end());
    it = input_lists[input_lists.size()-1].begin();
    push_heap(input_lists.begin(), input_lists.end(), greater<>{});
  }

  vector<int> output_list(n * m);

  cout << "test" << endl;
  // Attempt at multiway merge
  make_heap(input_lists.begin(), input_lists.end(), greater<>{});
  print(input_lists, n, m, "Beginning");

  cout << "test" << endl;
    for(size_t i = 0; i < output_list.size(); i++) {
      // cout << "test" << endl;
      output_list[i] = input_lists[0][0]; // Add smallest element to output_list
      // output_list[i] = input_lists[0][0];
      // cout << output_list[i] << " ";

      pop_heap(input_lists.begin(), input_lists.end()); // Pop smallest element to back
      // it = input_lists[input_lists.size()-1].begin(); // Set iterator to first element of last sublist
      // cout << "test ";
      input_lists[input_lists.size()-1].erase(input_lists[input_lists.size()-1].begin()); // Erase used value
      // cout << " test" << endl;
      
      if (input_lists[input_lists.size()-1].empty()) { // Check if sublist is empty after erase. If so, pop empty
        input_lists.pop_back();
      }

      make_heap(input_lists.begin(), input_lists.end(), greater<>{}); // Remake heap with new value from the sublist
    }

    // print(input_lists, n, m, "End");
  
  for (size_t i = 0; i < output_list.size(); i++)
    cout << output_list[i] << " ";
  cout << endl;

  return 0;
}

void print(vector< vector<int> > input_lists, int n, int m, string message) {
  cout << "   " << message << ":" << endl;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      cout << input_lists[i][j] << ' ';
    }
    cout << endl;
  }
}