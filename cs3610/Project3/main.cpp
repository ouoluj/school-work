/*
*
* Time complexity of constructing a Huffman Tree using a min heap is O(log(n))
* Time complexity of constructing a Huffman Tree without a min heap is O(n)
*
*/

#include <iostream>
#include "huffman_tree.h"
using namespace std;

int main(int argc, char** argv) {
  // Create a HuffmanTree object and read the input messages into the
  // HuffmanTree construct function. Next, print the encoded message.
  // Finally, destruct the huffman tree and move on to the next test case.

  HuffmanTree tree;
  string message = "";
  size_t cases = 0;

  cin >> cases; // Get number of cases
  cin.ignore(50, '\n'); // Clear trailing new line

  // Iterate through each test case
  for (size_t i = 0; i < cases; i++) {
    getline(cin, message); // Get message

    tree.construct(message); // Create tree for message
    cout << "Test Case: " << (i+1) << endl; // cout requested format
    tree.print(); // Print this encoded message
    tree.destruct(); // Destruct tree in preparation for next test case
  }

  return 0;
}
