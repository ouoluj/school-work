#include <vector>
#include <algorithm>
using namespace std;

template <typename T>
struct HeapNode {
  HeapNode(const T data, const int key) : data(data), key(key) {}
  bool operator<(const HeapNode<T>& rhs) {return this->key < rhs.key;}
  bool operator<=(const HeapNode<T>& rhs) {return this->key <= rhs.key;}

  T data;
  int key;
};

template <typename T>
class MinHeap {
public:
  MinHeap() {}
  ~MinHeap() {}

  void insert(const T data, const int key);
  T extract_min();
  T peek() const {return heap[0].data;};

  int size() const {return heap.size();};

private:
  void heapify(int root);
  vector<HeapNode<T> > heap;
};

// Inserts a new node to the heap
template <typename T>
void MinHeap<T>::insert(const T data, const int key) {
  // If it's the first node, simply add it to the vector
  if(heap.size() == 0) {
    heap.push_back(HeapNode<T>(data, key));
    return;
  }

  // Add node to vector
  heap.push_back(HeapNode<T>(data, key));
  size_t currentIndex = heap.size()-1; // Current index starting from the end
  size_t parentIndex = (currentIndex - 1)/2; // Current index's parent node
  HeapNode<T> temp(0, 0); // Temp node for swapping

  // Work backwards from the end of the vector, checking each node to see if it is
  // smaller than its parent. If it is, swap them, and continue along the vector.
  while (currentIndex != 0 && heap[currentIndex] < heap[parentIndex]) {
    // Swap nodes
    temp = heap[parentIndex];
    heap[parentIndex] = heap[currentIndex];
    heap[currentIndex] = temp;

    currentIndex = parentIndex; // Move current to its parent
    parentIndex = (currentIndex-1)/2; // Find new current's parent
  }
}

// Extracts current minimum node, and fixes the heap to ensure it is still valid
template <typename T>
T MinHeap<T>::extract_min() {
  if(heap.size() == 0) {T dud; return dud;}

  T min = heap[0].data; // Saves smallest node before overwriting

  heap[0] = heap[heap.size()-1]; // Move last node into first spot to save it
  heap.erase(heap.end()-1); // Delete last node to shorten vector

  heapify(0); // Fix the heap

  return min; // Return smallest node
}

template <typename T>
void MinHeap<T>::heapify(int root) {
  int smallest = root; // Current position to be heapified
  int left = (root * 2) + 1;  // Left child
  int right = (root * 2) + 2; // Right child

  if (left < heap.size() && heap[left] < heap[smallest]) {smallest = left;}

  if (right < heap.size() && heap[right] < heap[smallest]) {smallest = right;}

  if (smallest != root) {
    HeapNode<T> temp(0,0); // Temporary node used for swapping

    // Swap smallest node with current position
    temp = heap[root];
    heap[root] = heap[smallest];
    heap[smallest] = temp;

    // Heapify starting with next position
    heapify(smallest);
  }
}
