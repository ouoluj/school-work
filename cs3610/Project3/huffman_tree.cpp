#include <iostream>
#include "huffman_tree.h"
#include "min_heap.h"
using namespace std;

void HuffmanTree::construct(const string message) {
  this->message = message;

  // Count the frequency of each letter in message
  // e.g.
  //    message == "aaabbccccdd"
  //    frequencies == {a:3, b:2, c:4, d:2}
  map<char, int> frequency_map;
  for (int i = 0; i < message.length(); ++i) {
    if (frequency_map.find(message[i]) != frequency_map.end())
      ++frequency_map[message[i]];
    else
      frequency_map[message[i]] = 1;
  }

  // Create HuffmanNode for each unique letter in message
  // and organize nodes into a min heap
  // e.g.
  //  heap ==
  //            {b:2}
  //           /     \
  //        {d:2}   {a:3}
  //        /   \   /    \
  //      {c:4}
  MinHeap<HuffmanNode*> heap;
  map<char, int>::iterator it = frequency_map.begin();
  for (; it != frequency_map.end(); ++it) {
    HuffmanNode* node = new HuffmanNode(
      it->first, it->second
    );
    heap.insert(node, it->second);
  }

  // Combine nodes with smallest frequency and insert
  // back into heap until heap size == 1. Along the way,
  // create binary tree out of combined nodes.
  // e.g.
  //  (1)
  //     {b:2} == heap.extract_min()
  //     {d:2} == heap.extract_min()
  //     parent ==
  //               {*:4}
  //              /     \
  //            {b:2}  {d:2}
  //
  //     heap ==
  //              {a:3}
  //             /     \
  //           {c:4}   {*:4}
  //
  //  (2)
  //     {a:3} == heap.extract_min()
  //     {c:4} == heap.extract_min()
  //     parent ==
  //              {*:7}
  //             /     \
  //          {a:3}   {*:4}
  //
  //     heap ==
  //            {c:4}
  //           /
  //         {*:7}
  //
  //  (3)
  //     {*:4} == heap.extract_min()
  //     {*:7} == heap.extract_min()
  //     parent ==
  //            {*:11}
  //           /       \
  //      {c:4}        {*:7}
  //                  /     \
  //                {a:3}  {*:4}
  //                       /    \
  //                    {b:2}  {d:2}
  //
  //     heap == {*:11}
  while (heap.size() > 1) {
    HuffmanNode *left, *right;

    left = heap.extract_min();
    right = heap.extract_min();

    HuffmanNode *parent = new HuffmanNode(
      left->frequency + right->frequency
    );

    parent->left = left;
    parent->right = right;

    heap.insert(parent, parent->frequency);
  }

  // Get root of huffman tree. e.g. {*:11}
  this->root = heap.peek();
}


// For each character in the given message, search for character in the huffman tree,
// add it to the encoded string, add a space between characters, and then continue to
// the next character.
void HuffmanTree::print() const {

  // Print the Huffman encoding of this->message.
  // Append 0 to a character's encoding if moving left in Huffman tree.
  // Append 1 to a character's encoding if moving right in Huffman tree.

  // Remember, your Huffman tree is pointed at by this->root, so start your
  // character searches from there.

  // Also, feel free to add a print helper function.

  string code = "";

  for (size_t i = 0; i < message.size(); i++) {
    code += findCharacter(message[i], root); // Search for each character
    code.erase(code.length()-1); // Erase last character "y"
    code += " "; // Add space between each character
  }

  cout << code << endl; // Cout encoded string for this tree's message
}

// Print helper function
string HuffmanTree::findCharacter(char target, HuffmanNode* root)const {
  // The pointer reached null without finding the character in this subtree
  // Return "n" to indicate "not found"
  if(root == NULL) {return "n";}

  // The character we're searching for is at the current node
  // Return "y" to indicate "character found"
  else if(root->character == target) {
    return "y";
  }

  else {
    string code = "";

    // Add 0 for going left in the heap
    code += "0";
    code += findCharacter(target, root->left);;
    // If character was found, return up the stack
    if (code[code.length()-1] == 'y') {
      return code;
    }

    // If character wasn't found, remove both "not found" character and 0
    if (code[code.length()-1] == 'n') {
      code.erase(code.length()-1);
      code.erase(code.length()-1);
    }

    // Add 1 for going right in the heap
    code += "1";
    code += findCharacter(target, root ->right);
    // If character was found, return up the stack
    if (code[code.length()-1] == 'y') {
      return code;
    }

    // If character wasn't found, remove both "not found" character and 1
    if (code[code.length()-1] == 'n') {
      code.erase(code.length()-1);
      code.erase(code.length()-1);
    }
  }
  // Character was not found going either direction from this node, return "not found"
  return "n";
}
