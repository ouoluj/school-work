#include <iostream>
#include "linked_list.h"
using namespace std;

int main() {
  LinkedList<int> list;
  char select;
  int add;

  while (true) { // Continue until program is explicitly quit
    cin >> select;
    select = tolower(select);

    switch (select) {
      case 'a':
        cin >> add;
        list.push_front(add);
        break;

      case 'd':
        list.pop_front();
        break;

      case 'r':
        list.reverse();
        break;

      case 'p':
        list.print();
        break;

      case 'q':
        exit(0);

      default:
        cout << "Selection is not valid, please try again!" << endl;
        std::cin.ignore(100, '\n');
      } // End of switch statement
  } // End of infinite loop
} // End of main()
