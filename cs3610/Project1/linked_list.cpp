#include "linked_list.h"
#include <iostream>

template <typename T>
LinkedList<T>::~LinkedList() {
  ListNode* temp = head;
  while (temp != NULL) {
    temp = head -> next;
    delete head;
    head = temp;
  }
}

template <typename T>
void LinkedList<T>::push_front(const T data) {
  ListNode* temp = new ListNode(data, head);
  head = temp;
}

template <typename T>
void LinkedList<T>::pop_front() {
  if (head == NULL) {return;}

  ListNode* temp = head;
  head = head -> next;
  delete temp;
}

template <typename T>
void LinkedList<T>::reverse() {
  ListNode* current = head;
  ListNode* next = head;
  ListNode* prev = NULL;

  while (current != NULL) {
    // next = next->
  }
}

template <typename T>
void LinkedList<T>::print() const {
  ListNode* temp = head;
  while (temp != NULL) {
    std::cout << temp -> data;
    temp = temp -> next;
  }
}
