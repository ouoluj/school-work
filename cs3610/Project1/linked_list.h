#include <cstdlib>
#include <iostream>

template <typename T>
class LinkedList {
public:
  LinkedList() : head(NULL) {}

  ~LinkedList() {
    ListNode* temp = head;
    while (temp != NULL) {
      temp = head -> next;
      delete head;
      head = temp;
    }
  }


  void push_front(const T data) {
    ListNode* temp = new ListNode(data);
    temp -> next = head;
    head = temp;
  }

  void pop_front() {
    if (head == NULL) {
      std::cout << "Empty" << std::endl;
      return;
    }

    ListNode* temp = head;
    head = head -> next;
    delete temp;
  }

  void reverse() {
    if (head == NULL) {
      std::cout << "Empty" << std::endl;
      return;
    }
    
    ListNode* current = head;
    ListNode* next = NULL;
    ListNode* prev = NULL;

    while (current != NULL) {
      next = current -> next;
      current -> next = prev;
      prev = current;
      current = next;
    }
    head = prev;
  }

  void print() const  {
    if (head == NULL) {
      std::cout << "Empty" << std::endl;
      return;
    }

    ListNode* temp = head;
    while (temp != NULL) {
      std::cout << temp -> data << " ";
      temp = temp -> next;
    }
  std::cout << std::endl;
 }

private:
  struct ListNode {
    ListNode(const T data) : data(data), next(NULL) {}

    T data;
    ListNode* next;
  };

  ListNode* head;
};
