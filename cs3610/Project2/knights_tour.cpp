#include <iostream>
#include <iomanip>
#include "knights_tour.h"
using namespace std;

KnightsTour::KnightsTour(int board_size) {
  this->board_size = board_size;

  this->board.resize(board_size);
  for (int i = 0; i < board_size; ++i) {
    this->board[i].resize(board_size);
  }
}

void KnightsTour::print() {
  for (int i = 0; i < this->board_size; i++) {
    for (int j = 0; j < this->board_size; j++)
      cout << setw(4) << this->board[i][j] << " ";
    cout << endl;
  }
  cout << endl << endl;
}

// Function: get_moves()
//    Desc: Get the row and column indices of all valid
//          knight moves reachable from position row, col.
//          An invalid move would be one that sends the
//          knight off the edge of the board or
//          to a position that has already been visited.
//
//    int row         - Current row position of knight.
//    int col         - Current column position of knight.
//    int row_moves[] - Array to store row indices
//                      of all valid new moves reachable from
//                      the current position row, col.
//    int col_moves[] - Array to store column indices
//                      of all valid new moves reachable from
//                      the current position row, col.
//    int num_moves -   Number of valid moves found. Corresponds
//                      to the sizes of row_moves and col_moves.

void KnightsTour::get_moves (
  int row, int col,
  int row_moves[], int col_moves[], int& num_moves) {

    // Arrays to store potential moves before adding them to row_moves[] / col_moves[]
    int possibleRows[8];
    int possibleCols[8];

    // Right two, up one
    possibleRows[0] = row - 1;
    possibleCols[0] = col + 2;

    // Right two, down one
    possibleRows[1] = row + 1;
    possibleCols[1] = col + 2;

    // Down two, right one
    possibleRows[2] = row + 2;
    possibleCols[2] = col + 1;

    // Down two, left one
    possibleRows[3] = row + 2;
    possibleCols[3] = col - 1;

    // Left two, down one
    possibleRows[4] = row + 1;
    possibleCols[4] = col - 2;

    // Left two, up one
    possibleRows[5] = row - 1;
    possibleCols[5] = col - 2;

    // Up two, left one
    possibleRows[6] = row - 2;
    possibleCols[6] = col - 1;

    // Up two, right one
    possibleRows[7] = row - 2;
    possibleCols[7] = col + 1;


    int tempRow = row, tempCol = col;
    for (int i = 0; i < 8; i++) {
      tempCol = possibleCols[i];
      tempRow = possibleRows[i];
      // Check that i potential move is on the board
      if (tempCol < board_size && tempCol >= 0 &&
          tempRow < board_size && tempRow >= 0) {
        // If the board at current move has not been moved to, and is still 0,
        // add the current move as a valid move.
        if (board[tempRow][tempCol] == 0) {
          col_moves[num_moves] = tempCol;
          row_moves[num_moves] = tempRow;
          num_moves ++;
        }
      }
    }
}


// Function: move() --> Recursive
//     int row        - Current row position of knight.
//     int col        - Current column position of knight.
//     int& m         - Current move id in tour.
//                      Stored in board at position
//                      row, col.
//     int& num_tours - Total number of tours found.

void KnightsTour::move(int row, int col, int& m, int& num_tours) {

  if (m >= (board_size * board_size) - 1) { // Base case if all moves were valid and board is full.
    board[row][col] = m + 1; // Place knight at final position

    print(); // Print board for this tour
    num_tours++;

    board[row][col] = 0; // Reset current position for the next recursion
    return;
  }

  // Recursive case
  // Reinitialize variables for get_moves for the current position
  int row_moves[8], col_moves[8];
  int num_moves = 0;

  get_moves(row, col, row_moves, col_moves, num_moves); // Find all valid moves from current position

  // Go through each valid move from current position, and call move for each new position
  for (int i = 0; i < num_moves; i++) {
    m++;
    board[row][col] = m; // Place knight at current position
    move(row_moves[i], col_moves[i], m, num_tours);
    board[row][col] = 0; // Reset position for next recursion
    m--;
  }
}

int KnightsTour::generate(int row, int col) {
  int m = 0;
  int num_tours = 0;
  move(row, col, m, num_tours);

  return num_tours;
}
