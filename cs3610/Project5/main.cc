#include <iostream>
#include <stack>
#include <vector>
using namespace std;

void shortestPath(int vertex, int gSize, vector< vector<int> >& graph, vector<int>& Dist, vector<int>& previous);

int main() {
   int testCases = 0;   // Number of test cases
   int numCities = 0;   // Number of cities in current
   string city;         // Temporary string to store cities in names[]
   int distance;        // Temporary int to store distances in graph[][]
   
   cin >> testCases;
   for(int curr = 0; curr < testCases; curr++) {
      stack<string> path;  // Stack used for printing later

      cin >> numCities;    // Input number of cities in test case
      vector< vector<int> > graph(numCities, vector<int>(numCities)); // 2D array to store distance matrix
      vector<int> Dist(numCities);             // Array to store shortest distances from source
      vector<string> names(numCities);         // Array to store the names of each included city
      vector<int> previous(numCities);         // Array to store predesessors for each city

      // Input cities for this case
      cin.ignore(50, '\n'); // Flush trailing newline
      for (int i = 0; i < numCities; i++) {
         getline(cin, city);
         names[i] = city;   // Populate names array
      }

      // Input distance matrix
      for (int i = 0; i < numCities; i++) {
         for (int j = 0; j < numCities; j++) {
            cin >> distance;
            graph[i][j] = distance;
         }
      }

      // Populate arrays with distances and predecessors
      shortestPath(0, numCities, graph, Dist, previous);

      path.push(names[numCities-1]);      // Push destination city
      int current = numCities-1;          // Initiliaze cursor to step through previous[]
      while (names[0] != path.top()) {    // Populate shortest path stack until source city is reached
         current = previous[current];
         path.push(names[current]);
      }

      // Until shortest path stack is empty: cout next closest city; pop.
      while(!path.empty()) {
         cout << path.top() << " ";
         path.pop();
      }

      cout << Dist[numCities-1] << endl;  // cout the length of the shortest path
   } // End test case For
   
   return 0;
}  // End main


// Use Dijkstra’s Algorithm to find the shortest path from a given (source) vertex to a destination vertex.
// Dist[] stores shortest paths from vertex to each given 
void shortestPath(int vertex, int gSize, vector< vector<int> >& graph, vector<int>& Dist, vector<int>& previous) {
   // Populate Distances array
   for (int i = 0; i < gSize; i++) {
      if (graph[vertex][i] == 0 && vertex != i) { Dist[i] = TMP_MAX; }  // If vertex is not connected to city_i, set artificial infinite
      else { Dist[i] = graph[vertex][i]; }                              // If cities are connected, populate Dist[]
   }


   bool distFound[gSize]; // Array to keep track of which cities have confirmed shortest distances

   for (int i = 0; i < gSize; i++) { distFound[i] = false;} // All other weights are undetermined

   distFound[vertex] = true;   // The source is the first determined
   Dist[vertex] = 0;           // Initialize distance
   previous[vertex] = vertex;  // Initialize 

   double smallDist;    // Initialize distance of next closest vertex
   int current;         // Declare cursor variable

   for (int i = 0; i < gSize-1; i++) {
      smallDist = TMP_MAX;

      for (int j = 0; j < gSize; j++) {
         if (!distFound[j]) {
            if (Dist[j] < smallDist) {
               current = j;
               smallDist = Dist[current];
            }
         }
      }

      distFound[current] = true; // Set flag for most recently set vertex

      for (int j = 0; j < gSize; j++) {
         if (!distFound[j]) {
            if (smallDist + graph[current][j] < Dist[j] && graph[current][j] != 0) {
               Dist[j] = smallDist + graph[current][j];
               previous[j] = current;
            }
         }
      }
   } // End For
} // End shortestPath

























// void shortestPath(int vertex, int gSize, vector< vector<int> > graph, int Dist[], int previous[]) {
//    for (int i = 0; i < gSize; i++) { 
//       if (graph[vertex][i] == 0 && vertex != i) { Dist[i] = 99999; }
//       else { Dist[i] = graph[vertex][i]; } 
//       // Dist[i] = graph[vertex][i];   // Initialize weights away from the source
//       cout << Dist[i] << " ";
//    }

//    bool distFound[gSize];

//    for (int i = 0; i < gSize; i++) { distFound[i] = false;} // All other weights are undetermined

//    distFound[vertex] = true;   // The source is the first determined
//    Dist[vertex] = 0;   // Initialize distance
//    previous[vertex] = vertex;

//    double smallDist;  // Initialize distance of next closest vertex
//    int current = vertex;      // Storage for index to be finalized
//    int trail = vertex;          // Trailing index to assign predecessors

//    for (int i = 0; i < gSize-1; i++) {
//       smallDist = 99999;
      
//       // current = vertex;
//       for (int j = 0; j < gSize; j++) {
//          if (!distFound[j]) {
//             if (Dist[j] < smallDist) {
//                current = j;
//                smallDist = Dist[current];
//                previous[current] = trail;
//             }
//          }
//       }

//       cout << "C: " << current << endl;
//       cout << "T: " << trail << endl;
//       distFound[current] = true;
//       // previous[current] = trail;
//       trail = current;
      

//       for (int j = 0; j < gSize; j++) { // 
//          if (!distFound[j]) {
//             // cout << smallDist + graph[current][j] << endl;
//             // cout << Dist[j] << endl;
//             // previous[j] = current;
//             if (smallDist + graph[current][j] < Dist[j]) {
//                Dist[j] = smallDist + graph[current][j];
//                cout << Dist[j] << endl;
//                previous[j] = current;
//             }
//          }
//       }
//       // trail = current;
//       // cout << "message" << endl;
//    } // End For  
//    // cout << "message" << endl;
//    // return;
// } // End shortestPath



// Create 
// void shortestPath(int vertex, int gSize, vector< vector<int> > graph, int Dist[], int previous[]) {
//    for (int i = 0; i < gSize; i++) { 
//       if (graph[vertex][i] == 0 && vertex != i) { Dist[i] = 99999; }
//       else { Dist[i] = graph[vertex][i]; } 
//       // Dist[i] = graph[vertex][i];   // Initialize weights away from the source
//       cout << Dist[i] << " ";
//    }

//    bool distFound[gSize];

//    for (int i = 0; i < gSize; i++) { distFound[i] = false;} // All other weights are undetermined

//    distFound[vertex] = true;   // The source is the first determined
//    Dist[vertex] = 0;   // Initialize distance
//    previous[vertex] = vertex;

//    double smallDist;  // Initialize distance of next closest vertex
//    int current = vertex;      // Storage for index to be finalized
//    int trail = -1;          // Trailing index to assign predecessors

//    for (int i = 0; i < gSize-1; i++) {
//       smallDist = 99999;
//       trail = current;
//       // current = vertex;
//       for (int j = 0; j < gSize; j++) {
//          if (!distFound[j]) {
//             if (Dist[j] < smallDist || Dist[j] <= 0) {
//                current = j;
//                smallDist = Dist[current];
//             }
//          }
//       }

//       cout << "C: " << current << endl;
//       cout << "T: " << trail << endl;
//       distFound[current] = true;
//       previous[current] = trail;
      

//       for (int j = 0; j < gSize; j++) { // 
//          if (!distFound[j]) {
//             // cout << smallDist + graph[current][j] << endl;
//             // cout << Dist[j] << endl;
//             // previous[j] = current;
//             if (smallDist + graph[j][current] < Dist[j] || (Dist[j] <= 0)) {
//                Dist[j] = smallDist + graph[j][current];
//                cout << Dist[j] << endl;
//                // previous[current] = trail;
//             }
//          }
//       }
//       // trail = current;
//       // cout << "message" << endl;
//    } // End For  
//    // cout << "message" << endl;
//    // return;
// } // End shortestPath